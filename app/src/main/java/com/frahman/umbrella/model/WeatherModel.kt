package com.frahman.umbrella.model

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * Created by frahman on 9/2/17 for Umbrella.
 */

//data class WeatherModel()
data class CurrentWeather(val tempColor: Int, val temperature: String, val condition: String)

data class CurrentLocation(val location: String)

data class DailyWeather(val date: String, val day: String, val hourlyModels: MutableList<HourlyModel>)

data class HourlyModel(val timeCivil: String, val imageUrl: String, val temp: String)

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, imageUrl: String) {
    Glide.with(view.context).load(imageUrl).into(view)
}