package com.frahman.umbrella.model

/**
 * Created by frahman on 9/3/17 for Umbrella.
 */

data class LocationResponse(
		var results: List<Result>,
		var status: String// OK
)

data class Result(
		var address_components: List<Address_component>,
		var formatted_address: String,
		var geometry: Geometry,
		var place_id: String,// ChIJXezxrl7Aj4ARbqGvmu4yDZs
		var types: List<String>
)

data class Geometry(
		var bounds: Bounds,
		var location: Location,
		var location_type: String,// APPROXIMATE
		var viewport: Viewport
)

data class Bounds(
		var northeast: Northeast,
		var southwest: Southwest
)

data class Southwest(
		var lat: Double,// 37.5297739
		var lng: Double// -122.038506
)

data class Northeast(
		var lat: Double,// 37.606946
		var lng: Double// -121.926968
)

data class Location(
		var lat: Double,// 37.5641425
		var lng: Double// -122.004179
)

data class Viewport(
		var northeast: Northeast,
		var southwest: Southwest
)

data class Address_component(
		var long_name: String,// 94536
		var short_name: String,// 94536
		var types: List<String>
)
