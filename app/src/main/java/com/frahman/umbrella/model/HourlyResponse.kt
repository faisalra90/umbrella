package com.frahman.umbrella.model

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

data class HourlyResponse(
		var hourly_forecast: List<Hourly_forecast>,
		var error: ErrorMessage
)

data class Hourly_forecast(
		var FCTTIME: FCTTIME,
		var temp: Temp,
		var dewpoint: Dewpoint,
		var condition: String,// Clear
		var icon: String,// clear
		var icon_url: String,// http://icons.wxug.com/i/c/k/nt_clear.gif
		var fctcode: String,// 1
		var sky: String,// 2
		var wspd: Wspd,
		var wdir: Wdir,
		var wx: String,// Clear
		var uvi: String,// 0
		var humidity: String,// 51
		var windchill: Windchill,
		var heatindex: Heatindex,
		var feelslike: Feelslike,
		var qpf: Qpf,
		var snow: Snow,
		var pop: String,// 0
		var mslp: Mslp
)

data class Snow(
		var english: String,// 0.0
		var metric: String// 0
)

data class FCTTIME(
		var hour: String,// 2
		var hour_padded: String,// 02
		var min: String,// 00
		var min_unpadded: String,// 0
		var sec: String,// 0
		var year: String,// 2017
		var mon: String,// 9
		var mon_padded: String,// 09
		var mon_abbrev: String,// Sep
		var mday: String,// 1
		var mday_padded: String,// 01
		var yday: String,// 243
		var isdst: String,// 1
		var epoch: String,// 1504256400
		var pretty: String,// 2:00 AM PDT on September 01, 2017
		var civil: String,// 2:00 AM
		var month_name: String,// September
		var month_name_abbrev: String,// Sep
		var weekday_name: String,// Friday
		var weekday_name_night: String,// Friday Night
		var weekday_name_abbrev: String,// Fri
		var weekday_name_unlang: String,// Friday
		var weekday_name_night_unlang: String,// Friday Night
		var ampm: String,// AM
		var tz: String,//
		var age: String,//
		var UTCDATE: String//
)

data class Wspd(
		var english: String,// 1
		var metric: String// 2
)

data class Mslp(
		var english: String,// 29.78
		var metric: String// 1008
)

data class Heatindex(
		var english: String,// -9999
		var metric: String// -9999
)

data class Dewpoint(
		var english: String,// 51
		var metric: String// 11
)

data class Windchill(
		var english: String,// -9999
		var metric: String// -9999
)

data class Feelslike(
		var english: String,// 70
		var metric: String// 21
)

data class Qpf(
		var english: String,// 0.0
		var metric: String// 0
)

data class Temp(
		var english: String,// 70
		var metric: String// 21
)

data class Wdir(
		var dir: String,// NE
		var degrees: String// 43
)

data class ErrorMessage(
		var type: String,
		var description: String
)