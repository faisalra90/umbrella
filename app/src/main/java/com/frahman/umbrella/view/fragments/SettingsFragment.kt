package com.frahman.umbrella.view.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.PreferenceFragment
import android.widget.Toast
import com.frahman.umbrella.R
import com.frahman.umbrella.utils.Constants

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */
class SettingsFragment: PreferenceFragment(), SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.pref_weather)
    }

    override fun onResume() {
        super.onResume()

        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

        val preferencesMap = preferenceManager.sharedPreferences.all
        preferencesMap.entries
                .filterIsInstance<EditTextPreference>()
                .forEach { updateSummary(it) }
    }

    override fun onPause() {
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val preferencesMap = sharedPreferences.all

        // get the preference that has been changed
        val changedPreference = preferencesMap[key]
        // and if it's an instance of EditTextPreference class, update its summary
        if (preferencesMap[key] is EditTextPreference) {
            updateSummary(changedPreference as EditTextPreference)
        }

        if (key == Constants.PREF_ZIP_CODE && sharedPreferences.getString(Constants.PREF_ZIP_CODE, "").length != 5) {
            Toast.makeText(activity, "Incorrect Zip Code", Toast.LENGTH_LONG).show()
        }
    }

    private fun updateSummary(preference: EditTextPreference) {
        // set the EditTextPreference's summary value to its current text
        preference.summary = preference.text
    }
}