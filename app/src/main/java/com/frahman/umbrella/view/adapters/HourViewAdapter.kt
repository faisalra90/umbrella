package com.frahman.umbrella.view.adapters

import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.frahman.umbrella.R
import com.frahman.umbrella.databinding.ItemHourlyBinding
import com.frahman.umbrella.model.HourlyModel
import io.reactivex.annotations.NonNull

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

class HourViewAdapter(hourlyModels: List<HourlyModel>) : RecyclerView.Adapter<HourViewAdapter.ViewHolder>() {
    private var hourlyList: List<HourlyModel> = hourlyModels

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemHourlyBinding>(LayoutInflater.from(parent.context), R.layout.item_hourly, parent, false)
        return ViewHolder(binding, binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(hourlyList[position])
    }

    override fun getItemCount(): Int {
        return hourlyList.size
    }

    inner class ViewHolder(binding: ItemHourlyBinding, itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var mBinding: ItemHourlyBinding = binding

        fun bind(@NonNull hourlyModel: HourlyModel) {
            mBinding.hourlyModel = hourlyModel
        }
    }
}