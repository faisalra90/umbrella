package com.frahman.umbrella.view.viewUtils

import android.content.Context
import android.support.v7.app.AlertDialog
import com.frahman.umbrella.R

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

class DialogUtils{
    companion object {
        fun showDialog(context: Context, title: String, message: String): AlertDialog.Builder{
            val dialog = AlertDialog.Builder(context, R.style.DialogTheme)
            dialog.setTitle(title)
            dialog.setMessage(message)
            dialog.setCancelable(false)
            return dialog
        }
    }

}