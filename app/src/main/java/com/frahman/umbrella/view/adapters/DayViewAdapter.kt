package com.frahman.umbrella.view.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.frahman.umbrella.R
import com.frahman.umbrella.databinding.CardItemDayBinding
import com.frahman.umbrella.model.DailyWeather
import io.reactivex.annotations.NonNull


/**
 * Created by frahman on 8/31/17 for Umbrella.
 */
class DayViewAdapter(mList: List<DailyWeather>) : RecyclerView.Adapter<DayViewAdapter.ViewHolder>() {
    private var dailyList: List<DailyWeather> = mList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<CardItemDayBinding>(LayoutInflater.from(parent.context), R.layout.card_item_day, parent, false)
        val gridLayoutManager = GridLayoutManager(parent.context, 4)
        binding.hourlyRecycler.layoutManager = gridLayoutManager
        return ViewHolder(binding, binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dailyList[position])
    }

    override fun getItemCount(): Int {
        return dailyList.size
    }

    inner class ViewHolder(binding: CardItemDayBinding, itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mBinding: CardItemDayBinding = binding

        fun bind(@NonNull dayModel: DailyWeather) {
            mBinding.dailyWeatherModel = dayModel
            mBinding.hourlyRecycler.adapter = HourViewAdapter(dayModel.hourlyModels)
        }

    }
}