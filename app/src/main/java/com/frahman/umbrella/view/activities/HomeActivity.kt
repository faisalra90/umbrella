package com.frahman.umbrella.view.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.location.*
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.os.ResultReceiver
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.frahman.umbrella.R
import com.frahman.umbrella.UApplication
import com.frahman.umbrella.databinding.ActivityHomeBinding
import com.frahman.umbrella.model.CurrentLocation
import com.frahman.umbrella.model.CurrentWeather
import com.frahman.umbrella.model.DailyWeather
import com.frahman.umbrella.service.FetchAddressIntentService
import com.frahman.umbrella.utils.Constants
import com.frahman.umbrella.utils.DataStore
import com.frahman.umbrella.view.adapters.DayViewAdapter
import com.frahman.umbrella.view.viewUtils.DialogUtils
import com.frahman.umbrella.viewmodel.*
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), HomeViewModelContract.MainView, SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var homeActivityBinding: ActivityHomeBinding
        private set

    val homeViewModel = HomeViewModel(this@HomeActivity)

    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var sentToSettings = false

    override val context: Context
        get() = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()
        setSupportActionBar(toolbar)
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this)
        checkPermission()
    }

    override fun onDestroy() {
        super.onDestroy()
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                showSettings()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showSettings() {
        val intent = Intent(context, SettingsActivity::class.java)
        startActivity(intent)
    }

    private fun checkPermission() {
        dataStore = (this.application as UApplication).getDataStore()
        if (ActivityCompat.checkSelfPermission(context, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(context, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(context, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])) {
                //Showing Information about why we need the permission
                val builder = DialogUtils.showDialog(context,
                        getString(R.string.multiple_permission_title),
                        getString(R.string.multiple_permission_message))
                builder.setPositiveButton(getString(R.string.grant_button)) { dialog, _ ->
                    dialog.cancel()
                    ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
                }
                builder.setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.cancel() }
                builder.show()
            } else if (dataStore.getPermissionState(permissionsRequired[0])) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why we need the permission
                val builder = DialogUtils.showDialog(context,
                        getString(R.string.multiple_permission_title),
                        getString(R.string.multiple_permission_message))

                builder.setPositiveButton(getString(R.string.grant_button)) { dialog, _ ->
                    dialog.cancel()
                    sentToSettings = true
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivityForResult(intent, REQUEST_PERMISSION_SETTING)
                    Toast.makeText(baseContext, getString(R.string.goto_permission_toast), Toast.LENGTH_LONG).show()
                }
                builder.setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.cancel() }
                builder.show()
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
            }

            dataStore.setPermissionState(permissionsRequired[0], true)
        } else {
            //We already have the permission, carrying on.
            isGPSEnabled()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            var allgranted = false
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true
                } else {
                    allgranted = false
                    break
                }
            }

            if (allgranted) {
                isGPSEnabled()
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])) {
                val builder = AlertDialog.Builder(this, R.style.DialogTheme)
                builder.setTitle("Need Location Permission")
                builder.setMessage("This app needs Location permission.")
                builder.setCancelable(false)
                builder.setPositiveButton("Grant") { dialog, _ ->
                    dialog.cancel()
                    ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT)
                }
                builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
                builder.show()
            } else {
                Toast.makeText(baseContext, "Unable to get Permission", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                isGPSEnabled()
            }
        }
    }

    private fun isGPSEnabled() {
        val lm = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled = false
        var network_enabled = false

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user

            val dialog = DialogUtils.showDialog(context,
                    context.getString(R.string.location_required_title),
                    context.resources.getString(R.string.gps_network_not_enabled))
            dialog.setPositiveButton(context.resources.getString(R.string.open_location_settings), { _, _ ->
                sentToSettings = true
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                context.startActivity(myIntent)
            })
            dialog.setNegativeButton(context.getString(R.string.cancel), { _, _ ->
                if (!dataStore.zipCode.isEmpty())
                    homeViewModel.fetchLocationDetails(this, dataStore.zipCode, true)
                else
                    showSettings()
            })
            dialog.show()
        } else {
            getHourlyWeather()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getHourlyWeather() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = Constants.UPDATE_INTERVAL
        mLocationRequest.fastestInterval = Constants.FASTEST_INTERVAL

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                object : LocationCallback() {
                    override fun onLocationResult(locationResult: LocationResult?) {
                        super.onLocationResult(locationResult)
                        if (locationResult != null) {
                            startWeatherService(locationResult.lastLocation)
                            mFusedLocationClient.removeLocationUpdates(this)
                        } else {
                            mFusedLocationClient.lastLocation.addOnSuccessListener {
                                OnSuccessListener<Location> { location ->
                                    startWeatherService(location)
                                    mFusedLocationClient.removeLocationUpdates(this)
                                }
                            }.addOnFailureListener(
                                    { exception -> exception.printStackTrace() }
                            )
                        }
                    }
                }, null)
    }

    private fun startWeatherService(location: Location) {
        receiver = AddressResultReceiver(Handler())
        val intent = Intent(this, FetchAddressIntentService::class.java)
        intent.putExtra(Constants.RECEIVER, receiver)
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, location)
        startService(intent)
    }

    override fun loadData(weatherList: List<DailyWeather>) {
        homeActivityBinding.dailyRecycler.adapter = DayViewAdapter(weatherList)
    }

    override fun setCurrentLocation(currentLocation: CurrentLocation) {
        homeActivityBinding.currentLocation = currentLocation
    }

    override fun setCurrentWeather(currentWeather: CurrentWeather) {
        homeActivityBinding.currentWeather = currentWeather
    }

    private fun initDataBinding() {
        homeActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        val linearLayoutManager = LinearLayoutManager(this)
        homeActivityBinding.dailyRecycler.layoutManager = linearLayoutManager
    }

    open inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {

            // Display the address string
            // or an error message sent from the intent service.
            val mAddressOutput = resultData.getParcelable<Address>(Constants.RESULT_DATA_KEY) as Address

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                val titleString = mAddressOutput.locality + ", " + Constants.states[mAddressOutput.adminArea]
                setCurrentLocation(CurrentLocation(titleString))

                dataStore.setZipCode(mAddressOutput.postalCode)
                homeViewModel.fetchHourlyList(this@HomeActivity, mAddressOutput.postalCode)
            }
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, string: String?) {
        if (string == Constants.PREF_ZIP_CODE && sharedPreferences!!.getString(Constants.PREF_ZIP_CODE, "").length == 5) {
            homeViewModel.fetchLocationDetails(this, sharedPreferences.getString(Constants.PREF_ZIP_CODE, ""), false)
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        if (sentToSettings) {
            sentToSettings = false
            if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                isGPSEnabled()
            }
        }
    }

    companion object {
        private const val PERMISSION_CALLBACK_CONSTANT = 100
        private const val REQUEST_PERMISSION_SETTING = 101
        private lateinit var receiver: AddressResultReceiver
            private set
        private lateinit var dataStore: DataStore
            private set
        private var permissionsRequired = arrayOf(
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
    }
}