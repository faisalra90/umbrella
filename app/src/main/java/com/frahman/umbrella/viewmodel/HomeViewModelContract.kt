package com.frahman.umbrella.viewmodel

import android.content.Context
import com.frahman.umbrella.model.CurrentLocation
import com.frahman.umbrella.model.CurrentWeather
import com.frahman.umbrella.model.DailyWeather
import com.frahman.umbrella.model.Hourly_forecast

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

interface HomeViewModelContract {
    interface MainView {

        val context: Context
        fun loadData(weatherList: List<DailyWeather>)
        fun setCurrentWeather(currentWeather: CurrentWeather)
        fun setCurrentLocation(currentLocation: CurrentLocation)
    }

    interface ViewModel {
        fun destroy()
    }
}