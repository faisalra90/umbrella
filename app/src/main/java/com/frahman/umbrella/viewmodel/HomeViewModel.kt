package com.frahman.umbrella.viewmodel

import android.content.Context
import android.widget.Toast
import com.frahman.umbrella.BuildConfig
import com.frahman.umbrella.R
import com.frahman.umbrella.UApplication
import com.frahman.umbrella.model.*
import com.frahman.umbrella.service.LocationService
import com.frahman.umbrella.service.WeatherService
import com.frahman.umbrella.utils.UmbrellaUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers


/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

class HomeViewModel(mContext: Context) : HomeViewModelContract.ViewModel {

    private var context: Context? = mContext
    lateinit var hourlyList: MutableList<Hourly_forecast>
    private val weatherService = WeatherService.create()
    private val locationService = LocationService.create()
    private var _compoDis = CompositeDisposable()
    private val compoDis: CompositeDisposable
        get() {
            if (_compoDis.isDisposed) {
                _compoDis = CompositeDisposable()
            }
            return _compoDis
        }

    private fun manageSub(s: Disposable) = compoDis.add(s)

    fun fetchLocationDetails(callback: HomeViewModelContract.MainView, zipCode: String, status: Boolean) {
        manageSub(
                locationService.fetchLocationData(zipCode, status)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<LocationResponse>() {
                            override fun onNext(response: LocationResponse) {
                                val addressResponse = response.results[0]
                                if (addressResponse.address_components[addressResponse.address_components.size - 1].short_name == "US") {
                                    val address = addressResponse.formatted_address
                                    callback.setCurrentLocation(CurrentLocation(UmbrellaUtils.getFormattedCityState(address)))
                                    fetchHourlyList(callback, zipCode)
                                } else {
                                    Toast.makeText(context, "Please enter a US zip code.", Toast.LENGTH_LONG).show()
                                }
                            }

                            override fun onComplete() {}

                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                            }
                        })
        )
    }

    fun fetchHourlyList(callback: HomeViewModelContract.MainView, zipCode: String) {

        manageSub(
                weatherService.fetchHourlyReport(BuildConfig.API_KEY, zipCode)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableObserver<HourlyResponse>() {
                            override fun onNext(response: HourlyResponse) {
                                if (!response.hourly_forecast.isEmpty()) {
                                    callback.setCurrentWeather(
                                            getWeatherObject(response.hourly_forecast[0].temp, response.hourly_forecast[0].condition))
                                    hourlyList = response.hourly_forecast as MutableList<Hourly_forecast>
                                    callback.loadData(getDailyWeatherList(response.hourly_forecast))
                                } else {
                                    onError(Throwable())
                                    Toast.makeText(context, response.error.description, Toast.LENGTH_LONG).show()
                                }
                            }

                            override fun onComplete() {

                            }

                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                            }
                        })
        )
    }

    private fun getDailyWeatherList(hourly_forecasts: List<Hourly_forecast>): List<DailyWeather> {
        val weatherList = mutableListOf<DailyWeather>()
        val dailyMap = hashMapOf<String, DailyWeather>()
        for (hourly_forecast in hourly_forecasts) {

            val date =
                    hourly_forecast.FCTTIME.year + "-" +
                    hourly_forecast.FCTTIME.mon_padded + "-" +
                    hourly_forecast.FCTTIME.mday_padded + " " +
                            hourly_forecast.FCTTIME.hour_padded + ":" +
                            hourly_forecast.FCTTIME.min + ":" +
                            hourly_forecast.FCTTIME.sec

            val relativeDay = UmbrellaUtils.getRelativeTimeSpanString(date)

            if (!dailyMap.containsKey(relativeDay)) {
                val hourModels = mutableListOf<HourlyModel>()
                hourModels.add(HourlyModel(hourly_forecast.FCTTIME.civil, hourly_forecast.icon_url, getScaledTemperature(hourly_forecast.temp)))
                dailyMap.put(relativeDay, DailyWeather(date, relativeDay, hourModels))
            } else {
                dailyMap[relativeDay]!!.hourlyModels.add(HourlyModel(hourly_forecast.FCTTIME.civil, hourly_forecast.icon_url, getScaledTemperature(hourly_forecast.temp)))
            }
        }

            if (dailyMap.containsKey("Today"))
                weatherList.add(0, dailyMap["Today"]!!)
            if (dailyMap.containsKey("Tomorrow")) {
                if(weatherList.size >= 1)
                    weatherList.add(1, dailyMap["Tomorrow"]!!)
                else
                    weatherList.add(0, dailyMap["Tomorrow"]!!)
            }
            if (dailyMap.containsKey("In 2 days")) {
                if (weatherList.size >= 2)
                    weatherList.add(2, dailyMap["In 2 days"]!!)
                else
                    weatherList.add(1, dailyMap["In 2 days"]!!)
            }
        return weatherList
    }

    private fun getScaledTemperature(temp: Temp): String {
        return if ((context!!.applicationContext as UApplication).getDataStore().tempScale == "Fahrenheit")
            temp.english + "\u00b0"
        else
            temp.metric + "\u00b0"
    }

    private fun getWeatherObject(temp: Temp, condition: String): CurrentWeather {
        val color = if (temp.english.toInt() > 60)
            context!!.resources.getColor(R.color.colorHot)
        else
            context!!.resources.getColor(R.color.colorCool)

        return CurrentWeather(color, getScaledTemperature(temp), condition)
    }

    private fun reset() {
        compoDis.clear()
        compoDis.dispose()
        context = null
    }

    override fun destroy() {
        reset()
    }
}