package com.frahman.umbrella.utils

import android.text.format.DateUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

class UmbrellaUtils {

    companion object {

        fun getFormattedCityState(address: String): String {
            val pattern = Pattern.compile("^\\D*(\\d)")
            val matcher = pattern.matcher(address)
            matcher.find()
            return address.substring(0, matcher.start(1)
            )
        }

        fun getRelativeTimeSpanString(originalDate: String): String{
            return try {
                val now = System.currentTimeMillis()
                val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
                val convertedDate = dateFormat.parse(originalDate)
                val relativeTime = DateUtils.getRelativeTimeSpanString(convertedDate.time, now, DateUtils.DAY_IN_MILLIS)
                relativeTime.toString()
            } catch (e: Exception) {
                e.printStackTrace()
                ""
            }
        }

    }
}