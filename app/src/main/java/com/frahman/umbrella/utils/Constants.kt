package com.frahman.umbrella.utils

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */
class Constants {
    companion object {

        val UPDATE_INTERVAL = (10 * 1000).toLong()  /* 10 secs */
        val FASTEST_INTERVAL: Long = 2000 /* 2 sec */
        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        const val PACKAGE_NAME = "com.google.android.gms.location.sample.locationaddress"
        const val RECEIVER = PACKAGE_NAME + ".RECEIVER"
        const val RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA"

        const val PREF_ZIP_CODE = "PREF_ZIP_CODE"
        const val PREF_TEMP_SCALE = "PREF_TEMP_SCALE"
        var states = hashMapOf(
                "Alabama" to "AL",
                "Alaska" to "AK",
                "Alberta" to "AB",
                "American Samoa" to "AS",
                "Arizona" to "AZ",
                "Arkansas" to "AR",
                "Armed Forces (AE)" to "AE",
                "Armed Forces Americas" to "AA",
                "Armed Forces Pacific" to "AP",
                "British Columbia" to "BC",
                "California" to "CA",
                "Colorado" to "CO",
                "Connecticut" to "CT",
                "Delaware" to "DE",
                "District Of Columbia" to "DC",
                "Florida" to "FL",
                "Georgia" to "GA",
                "Guam" to "GU",
                "Hawaii" to "HI",
                "Idaho" to "ID",
                "Illinois" to "IL",
                "Indiana" to "IN",
                "Iowa" to "IA",
                "Kansas" to "KS",
                "Kentucky" to "KY",
                "Louisiana" to "LA",
                "Maine" to "ME",
                "Manitoba" to "MB",
                "Maryland" to "MD",
                "Massachusetts" to "MA",
                "Michigan" to "MI",
                "Minnesota" to "MN",
                "Mississippi" to "MS",
                "Missouri" to "MO",
                "Montana" to "MT",
                "Nebraska" to "NE",
                "Nevada" to "NV",
                "New Brunswick" to "NB",
                "New Hampshire" to "NH",
                "New Jersey" to "NJ",
                "New Mexico" to "NM",
                "New York" to "NY",
                "Newfoundland" to "NF",
                "North Carolina" to "NC",
                "North Dakota" to "ND",
                "Northwest Territories" to "NT",
                "Nova Scotia" to "NS",
                "Nunavut" to "NU",
                "Ohio" to "OH",
                "Oklahoma" to "OK",
                "Ontario" to "ON",
                "Oregon" to "OR",
                "Pennsylvania" to "PA",
                "Prince Edward Island" to "PE",
                "Puerto Rico" to "PR",
                "Quebec" to "PQ",
                "Rhode Island" to "RI",
                "Saskatchewan" to "SK",
                "South Carolina" to "SC",
                "South Dakota" to "SD",
                "Tennessee" to "TN",
                "Texas" to "TX",
                "Utah" to "UT",
                "Vermont" to "VT",
                "Virgin Islands" to "VI",
                "Virginia" to "VA",
                "Washington" to "WA",
                "West Virginia" to "WV",
                "Wisconsin" to "WI",
                "Wyoming" to "WY",
                "Yukon Territory" to "YT")
    }


}