package com.frahman.umbrella.utils

import android.preference.PreferenceManager
import com.frahman.umbrella.UApplication

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */


class DataStore private constructor() {

    val zipCode: String
        get() {
            return PreferenceManager.getDefaultSharedPreferences(uApplication!!.applicationContext).getString(Constants.PREF_ZIP_CODE, "")
        }

    fun setZipCode(zipCode: String) {
        PreferenceManager.getDefaultSharedPreferences(uApplication!!.applicationContext).edit().putString(Constants.PREF_ZIP_CODE, zipCode).apply()
    }

    val tempScale: String
        get() {
            return PreferenceManager.getDefaultSharedPreferences(uApplication!!.applicationContext).getString(Constants.PREF_TEMP_SCALE, "Fahrenheit")
        }

    fun setTempScale(tempScale: String) {
        PreferenceManager.getDefaultSharedPreferences(uApplication!!.applicationContext).edit().putString(Constants.PREF_TEMP_SCALE, tempScale).apply()
    }

    fun getPermissionState(permission: String): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(uApplication!!.applicationContext).getBoolean(permission, false)
    }

    fun setPermissionState(permission: String, value: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(uApplication!!.applicationContext).edit().putBoolean(permission, value).apply()
    }

    companion object {

        private var uApplication: UApplication? = null

        private val appInstance = DataStore()

        fun getInstance(application: UApplication): DataStore {
            uApplication = application
            return appInstance
        }
    }
}
