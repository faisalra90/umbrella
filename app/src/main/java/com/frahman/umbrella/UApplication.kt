package com.frahman.umbrella

import android.app.Application
import android.content.Context
import com.frahman.umbrella.utils.DataStore
import com.frahman.umbrella.service.WeatherService
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
* Created by frahman on 8/31/17 for Umbrella for Umbrella.
*/

class UApplication : Application(){

    private operator fun get(context: Context): UApplication {
        return context.applicationContext as UApplication
    }

    fun getWeatherService(): WeatherService {
        if (weatherService == null) weatherService = WeatherService.create()
        return weatherService as WeatherService
    }

    fun subscribeScheduler(): Scheduler {
        if (scheduler == null) scheduler = Schedulers.io()
        return scheduler as Scheduler
    }

    fun setWeatherService(mWeatherService: WeatherService) {
        weatherService = mWeatherService
    }

    fun setScheduler(mScheduler: Scheduler) {
        scheduler = mScheduler
    }

    override fun onCreate() {
        super.onCreate()
    }

    fun getDataStore(): DataStore {
        return DataStore.getInstance(this)
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    companion object {
        private var weatherService: WeatherService? = null
        private var scheduler: Scheduler? = null
    }
}