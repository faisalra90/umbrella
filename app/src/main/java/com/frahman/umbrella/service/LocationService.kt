package com.frahman.umbrella.service

import com.frahman.umbrella.model.HourlyResponse
import com.frahman.umbrella.model.LocationResponse
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

interface LocationService {

    @Headers("Accept: */*")
    @GET("geocode/json")
    fun fetchLocationData(@Query("address") type: String,
                          @Query("sensor") status: Boolean) : Observable<LocationResponse>

    companion object {
        private const val BASE_URL = "http://maps.googleapis.com/maps/api/"

        fun create() : LocationService {
            val gsonBuilder = GsonBuilder()

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()

            val restAdapter = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .client(client)
                    .build()

            return restAdapter.create(LocationService::class.java)
        }
    }
}