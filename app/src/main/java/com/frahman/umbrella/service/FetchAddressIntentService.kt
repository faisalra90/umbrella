package com.frahman.umbrella.service

import android.annotation.SuppressLint
import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.os.ResultReceiver
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.frahman.umbrella.R
import com.frahman.umbrella.utils.Constants
import java.io.IOException
import java.util.*


/**
 * Created by frahman on 8/31/17 for Umbrella.
 */

open class FetchAddressIntentService : IntentService(null) {

    override fun onHandleIntent(intent: Intent?) {
        // Get the location passed to this service through an extra.
        val location = intent?.getParcelableExtra<Parcelable>(Constants.LOCATION_DATA_EXTRA) as Location
        mReceiver = intent.getParcelableExtra<Parcelable>(Constants.RECEIVER) as ResultReceiver

        getSystemService(AppCompatActivity.LOCATION_SERVICE)
        val lat = 0.00
        val lng = 0.00
        var errorMessage = ""

        val geocoder = Geocoder(this, Locale.getDefault())
        var addresses: List<Address>? = geocoder.getFromLocation(lat, lng, 1)
        try {
            addresses = geocoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    // In this sample, get just a single address.
                    1)
        } catch (ioException: IOException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available)
            Log.e(TAG, errorMessage, ioException)
        } catch (illegalArgumentException: IllegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used)
            Log.e(TAG, errorMessage + " " +
                    "Latitude = " + location.latitude +
                    ", Longitude = " +
                    location.longitude, illegalArgumentException)
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.isEmpty()) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found)
                Log.e(TAG, errorMessage)
            }
            deliverResultToReceiver(Constants.FAILURE_RESULT, errorMessage)
        } else {
            deliverResultToReceiver(Constants.SUCCESS_RESULT, addresses[0])
        }
    }

    @SuppressLint("RestrictedApi")
    private fun deliverResultToReceiver(resultCode: Int, errorMessage: String) {
        val bundle = Bundle()
        bundle.putString(Constants.RESULT_DATA_KEY, errorMessage)
        mReceiver!!.send(resultCode, bundle)
    }

    @SuppressLint("RestrictedApi")
    private fun deliverResultToReceiver(resultCode: Int, address: Address) {
        val bundle = Bundle()
        bundle.putParcelable(Constants.RESULT_DATA_KEY, address)
        mReceiver!!.send(resultCode, bundle)
    }

    companion object {
        private val TAG = "Address Service"
        private var mReceiver: ResultReceiver? = null
    }
}
