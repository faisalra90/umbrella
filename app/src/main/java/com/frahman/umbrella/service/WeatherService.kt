package com.frahman.umbrella.service

import com.frahman.umbrella.model.HourlyResponse
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


/**
* Created by frahman on 8/31/17 for Umbrella for Umbrella.
*/

interface WeatherService {

        @Headers("Accept: */*")
        @GET("{key}/hourly/q/{zipCode}.json")
        fun fetchHourlyReport(@Path("key") type: String,
                      @Path("zipCode") zipCode: String) : Observable<HourlyResponse>

        companion object {
            private const val BASE_URL = "http://api.wunderground.com/api/"

            fun create() : WeatherService {
                val gsonBuilder = GsonBuilder()

                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

                val client = OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()

                val restAdapter = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                        .client(client)
                        .build()

                return restAdapter.create(WeatherService::class.java)
            }
        }
}